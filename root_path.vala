private unowned string? left_path (string path) {
	unowned uint8[] data = path.data;
	int length = path.length;

	if (data[length - 1] == '/')
		data[length - 1] = ' ';
	var offset = path.last_index_of_char('/');
	if (offset == -1)
		return null;
	data[offset] = '\0';
	return path;
}

public string get_root_project (out ProjectType type) {
	string PWD = Environment.get_variable ("PWD"); 
	unowned string HOME = Environment.get_variable ("HOME");
	unowned string? path = PWD;
	string? git_dir = null;
	
	type = ProjectType.NONE;
	while (HOME != path && path != "/tmp" && path != null)
	{
		//		Makefile Build part
		if (FileUtils.test(path + "/Makefile", FileTest.EXISTS)) {
			type = ProjectType.MAKEFILE;
			return path;
		}

		//		Meson Build part
		else if (FileUtils.test(path + "/meson.build", FileTest.EXISTS)) {
			// search a true meson.build
			unowned string ptr = path;
			string? before = ptr;
			while (true) {
				before = ptr;
				ptr = left_path(ptr);
				if (ptr == null)
					break;
				if (ptr.has_suffix("/subprojects"))
					continue;
				else if (FileUtils.test(ptr + "/meson.build", FileTest.EXISTS)) {
					continue;
				}
				break;
			}
			path = before;
			type = ProjectType.MESON;
			return path;
		}
		if (FileUtils.test(path + "/.git", FileTest.EXISTS))
			git_dir = path;
		path = left_path(path);
	}

	if (git_dir != null)
		return git_dir;
	return Environment.get_variable ("PWD");
}
