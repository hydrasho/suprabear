public enum Compiler {
	VALAC,
	C,
	CPP,
	NONE
}

public struct InfoCompile {
	Compiler compiler;
	string? output;
	string? file;
	unowned string line;
}

private bool is_compiler_c (string str){
	if (str.has_prefix("cc"))
		return true;
	else if (str.has_prefix("clang"))
		return true;
	else if (str.has_prefix("gcc"))
		return true;
	return false;
}

private bool is_compiler_cpp (string str){
	if (str.has_prefix("c++"))
		return true;
	else if (str.has_prefix("g++"))
		return true;
	else if (str.has_prefix("clang++"))
		return true;
	else if (str.has_prefix("cpp"))
		return true;
	else if (str.has_prefix("cxx"))
		return true;
	return false;
}

public InfoCompile analize_line (string line) throws Error
{
	InfoCompile info;
	MatchInfo match;

	string? file = null;
	string? output = null;

	// Check if the line is a compiler
	Compiler compiler = NONE;
	if (line.has_prefix("valac"))
		compiler = Compiler.VALAC;
	else if (is_compiler_c(line))
		compiler = Compiler.C;
	else if (is_compiler_cpp(line))
		compiler = Compiler.CPP;

	// Check if the line has output and file
	if (reg_output.match(line, 0, out match))
		output = match.fetch (1);

	// Check if the line has file
	if (reg_files.match(line, 0, out match))
		file = match.fetch (1);

	info = {
		compiler,
		(owned)output,
		(owned)file,
		line
	};
	return info;
}
