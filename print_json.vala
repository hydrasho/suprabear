private struct JsonData {
	string? directory;
	string? []arguments;
	string? command;
	unowned InfoCompile info;

	public JsonData (InfoCompile info) {
		this.info = info;
		if (info.compiler == VALAC) {
			command = info.line;
		}
		else {
			arguments = info.line.split(" ");
		}
		directory = PWD;
	}

	public void write(StringBuilder stream) {
		if (info.file != null) {
			stream.append("{\n");
			if (directory != null)
				stream.append_printf("\t\"directory\": \"%s\",\n", directory);
			if (command != null)
				stream.append_printf("\t\"command\": \"%s\",\n", command.escape());
			else if (arguments != null) {
				stream.append("""	"arguments": [""");
				foreach (unowned string av in arguments) {
					if (av != "")
						stream.append_printf("\"%s\", ", av.escape());
				}
				stream.truncate(stream.len -2);
				stream.append("],\n");
			}
			if (info.file != null)
				stream.append_printf("\t\"file\": \"%s\",\n", info.file);
			if (info.output != null && info.compiler != VALAC)
				stream.append_printf("\t\"output\": \"%s\"\n", info.output);
			else
				stream.append("\t\"output\": \"main.c\"\n");
			stream.append_c('}');
		}
	}
}


public string make_json (string []cmds) throws Error {
	var stream = new StringBuilder.sized(1024);

	stream.append("[\n");
	foreach (unowned var line in cmds) {
		var info = analize_line (line);
		if (info.compiler == NONE)
			continue;

		JsonData data = JsonData(info);
		data.write(stream);
		if (info.file != null)
			stream.append(",\n");
	}
	stream.truncate (stream.len -2);
	stream.append("\n]\n");
	return (owned)stream.str;
}

