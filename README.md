# Installation

## with meson.build

```bash
meson build --prefix=~/.local

ninja install -C build
```

## with Suprapack

```bash
meson build --prefix=$PWD/usr
ninja install -C build
suprapack install ./le_fichier_suprapack_generer.suprapack
```

## with Makefile only local (~/.local)

```bash
make
make install
```


# How to use

```bash
suprabear
```

```bash
suprabear -n > compile_commands.json
```

```bash
suprabear --stdin << end
gcc main.c -c -o main.o
gcc hello.c -c -o hello.o
gcc salut.c -c -o salut.o
end
```
