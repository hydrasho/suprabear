public unowned string PWD;
public unowned string HOME;
public Regex reg_output;
public Regex reg_files;

public class Config {
	public Config (string []args) throws Error {
		var opt_context = new OptionContext ("- OptionContext example");
		opt_context.set_help_enabled (true);
		opt_context.add_main_entries (options, null);
		opt_context.parse (ref args);

		if (Config.version == true) {
			print ("version: %s\n", Extern.VERSION);
		}
	} 

	public static bool display = false;
	public static bool stdin = false;
	public static bool ignore = false;
	public static bool silent = false;
	public static bool project_path = false;
	public static bool version = false;

	private const GLib.OptionEntry[] options = {
		{ "project-path", 'p', OptionFlags.NONE, OptionArg.NONE, ref project_path, "just print the root path project", null },
		{ "stdin", 'i', OptionFlags.NONE, OptionArg.NONE, ref stdin, "look stdin stream", null },
		{ "silent", 's', OptionFlags.NONE, OptionArg.NONE, ref silent, "never print output or errput (--display is not affected)", null },
		{ "gitignore", 'g', OptionFlags.NONE, OptionArg.NONE, ref ignore, "generate a .gitignore file", null },
		{ "display", 'n', OptionFlags.NONE, OptionArg.NONE, ref display, " display and dont create the compile_command.json", "INT" },
		{ "version", 'v', OptionFlags.NONE, OptionArg.NONE, ref version, "print version", null },
		{ null }
	};
}



string[]? get_make_cmd (out string root_dir) throws Error {
	root_dir = PWD;
	string? output;
	string? errput;

	while (HOME != root_dir && root_dir != "/tmp" && root_dir != null) {
		if (FileUtils.test(root_dir + "/Makefile", FileTest.EXISTS)) {
			if (FileUtils.test(root_dir + "/meson.build", FileTest.EXISTS)) {
				FileUtils.symlink (@"$root_dir/build/compile_commands.json", @"$root_dir/compile_commands.json");
				return null;
			}
			Process.spawn_sync (root_dir, {"make", "-n", "-B"}, null, SpawnFlags.SEARCH_PATH, null, out output, out errput);
			if (output == null)
				throw new FileError.FAILED("Error to get make command %d", Log.LINE); 
			if (Config.ignore == true) {
				string gitignore_path = root_dir + "/.gitignore";
				string contents;

				if (FileUtils.test (gitignore_path, FileTest.EXISTS) == true)
					FileUtils.get_contents (gitignore_path, out contents);
				else
					contents = "";
				if (contents.index_of("compile_commands.json") == -1)
					contents += "\ncompile_commands.json";
				FileUtils.set_contents (gitignore_path, contents);
			}
			if (Config.silent == false)
				print (errput);
			return output.split("\n");
		}
		root_dir = Path.get_dirname (root_dir);
	}	
	return null;
}

public enum ProjectType {
	NONE = 0,
	MAKEFILE = 1,
	MESON = 2,
}

int main (string []argv) {
	PWD = Environment.get_variable ("PWD");
	HOME = Environment.get_variable ("HOME");

	try {
		string []cmds;
		new Config (argv);


		if (Config.project_path == true) {
			ProjectType type;
			print (get_root_project (out type));
			return type;
		}

		reg_output = /-o\s([^\s]+)/;
		reg_files = /([^\s]+?[.](vala|c|cpp|cxx]))/;

		string? root_dir = PWD;
		if (Config.stdin == true) {
			string? line = null;
			var strv = new StrvBuilder ();
			
			while ((line = stdin.read_line ()) != null)
				strv.add (line);
			cmds = strv.end ();
		}
		else
			cmds = get_make_cmd(out root_dir);

		if (cmds != null) {
			var str = make_json(cmds);
			if (Config.display == true)
				print (str);
			else
				FileUtils.set_contents (@"$root_dir/compile_commands.json", str);
		}
	}

	catch (Error e) {
		if (Config.silent == false)
			printerr("Error: %s\n", e.message);
	}
	return 0;
}
